import './products-table.styles.css';

import {
  Box,
  IconButton,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from '@mui/material';
import {
  Item,
  getProducts,
  removeProduct,
  setProduct,
} from '../../utils/local-storage';
import React, { ReactElement, useEffect, useState } from 'react';

import AddIcon from '@mui/icons-material/Add';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';
import RemoveIcon from '@mui/icons-material/Remove';

const ProductsTable: React.FC<{
  setProductsChanged: any;
}> = ({ setProductsChanged }: { setProductsChanged: any }): ReactElement => {
  const [items, setItems] = useState<Item[]>([]);
  const [total, setTotal] = useState<number>(0);

  useEffect(() => {
    setItems(getProducts());
  }, []);

  useEffect(() => {
    let calculatedTotal = 0;
    items.forEach((item) => {
      calculatedTotal += item.quantity * item.product.price;
    });
    setTotal(calculatedTotal);
  }, [items]);

  const handleClickRemove = (productId: number): void => {
    removeProduct(productId);
    setItems(getProducts());
    setProductsChanged(Math.random());
  };

  const handleClickChangeQuantity = (
    operation: string,
    productId: number
  ): void => {
    const item = Object.assign(
      {},
      items.find((item) => item.product.id === productId)
    );
    if (operation === 'add') {
      item.quantity++;
    } else {
      item.quantity = item.quantity > 1 ? item.quantity - 1 : 1;
    }
    setProduct(item);
    setItems(getProducts());
    setProductsChanged(Math.random());
  };

  return (
    <TableContainer component={Paper}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Producto</TableCell>
            <TableCell>Precio Unitario</TableCell>
            <TableCell>Cantidad</TableCell>
            <TableCell>Precio Total</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {items.map((item) => (
            <TableRow key={item.product.id}>
              <TableCell component="th" scope="row">
                <IconButton
                  size="small"
                  color="error"
                  onClick={() => handleClickRemove(item.product.id)}
                >
                  <RemoveCircleIcon />
                </IconButton>
                {item.product.name}
              </TableCell>
              <TableCell align="right">${item.product.price}</TableCell>
              <TableCell align="right">
                <Box
                  sx={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'flex-end',
                  }}
                >
                  <IconButton
                    color="inherit"
                    onClick={() =>
                      handleClickChangeQuantity('remove', item.product.id)
                    }
                  >
                    <RemoveIcon />
                  </IconButton>
                  <Typography variant="body1">{item.quantity}</Typography>
                  <IconButton
                    color="inherit"
                    onClick={() =>
                      handleClickChangeQuantity('add', item.product.id)
                    }
                  >
                    <AddIcon />
                  </IconButton>
                </Box>
              </TableCell>
              <TableCell align="right">
                ${item.quantity * item.product.price}
              </TableCell>
            </TableRow>
          ))}
          <TableRow>
            <TableCell>Total</TableCell>
            <TableCell align="right" colSpan={3}>
              ${total}
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default ProductsTable;
