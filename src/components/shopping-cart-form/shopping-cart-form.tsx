import './shopping-cart-form.styles.css';

import {
  Button,
  FormControl,
  FormHelperText,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
  TextField,
  Typography,
} from '@mui/material';
import { Form, Formik } from 'formik';
import { Item, getProducts } from '../../utils/local-storage';
import React, { ReactElement, useEffect, useState } from 'react';

import { City } from '../../data/types';
import citiesJson from '../../data/cities.json';
import formValidationSchema from './form-validation-schema';

const initialValues = {
  city: '',
  street: '',
  streetNumber: '',
  reference: '',
  paymentMethod: 'cash',
  cashValue: '',
  cardNumber: '',
  cardNames: '',
  cardDueDate: '',
  cardCvc: '',
  shipMethod: 'asap',
  shipDate: '',
};

const ShoppingCartForm: React.FC<{ productsChanged: number }> = ({
  productsChanged,
}: {
  productsChanged: number;
}): ReactElement => {
  const [cities, setCities] = useState<City[]>([]);
  const [paymentMethod, setPaymentMethod] = useState('cash');
  const [shipType, setShipType] = useState('asap');
  const [products, setProducts] = useState<Item[]>([]);
  const [totalOrder, setTotalOrder] = useState<number>(0);

  useEffect(() => {
    setProducts(getProducts());
  }, [productsChanged]);

  useEffect(() => {
    setCities(citiesJson);
  }, []);

  useEffect(() => {
    let total = 0;
    products.forEach((product) => {
      total += product.quantity * product.product.price;
    });
    setTotalOrder(total);
  }, [products]);

  const handleChangePaymentMethod = (
    event: SelectChangeEvent,
    formikChange: any
  ) => {
    formikChange(event);
    setPaymentMethod(event.target.value);
  };

  const handleChangeShipType = (
    event: SelectChangeEvent,
    formikChange: any
  ) => {
    formikChange(event);
    setShipType(event.target.value);
  };

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={formValidationSchema}
      onSubmit={() => alert('Pedido realizado correctamente')}
    >
      {(formik) => (
        <Form onSubmit={formik.handleSubmit}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Typography variant="h5" sx={{ mb: 2, mt: 5 }}>
                Dirección de entrega
              </Typography>
            </Grid>

            <Grid item xs={12}>
              <FormControl
                fullWidth
                error={Boolean(formik.errors.city && formik.touched.city)}
              >
                <InputLabel id="city-label">Ciudad</InputLabel>
                <Select
                  labelId="city-label"
                  id="city"
                  name="city"
                  value={formik.values.city}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                >
                  {cities &&
                    cities.map((city, index) => (
                      <MenuItem key={index} value={city.id}>
                        {city.name}
                      </MenuItem>
                    ))}
                </Select>
                <FormHelperText>
                  {formik.errors.city && formik.touched.city
                    ? formik.errors.city
                    : ''}
                </FormHelperText>
              </FormControl>
            </Grid>

            <Grid item xs={8}>
              <TextField
                label="Calle"
                fullWidth
                id="street"
                name="street"
                value={formik.values.street}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={Boolean(formik.errors.street && formik.touched.street)}
                helperText={
                  formik.errors.street && formik.touched.street
                    ? formik.errors.street
                    : ''
                }
              />
            </Grid>

            <Grid item xs={4}>
              <TextField
                label="Número"
                fullWidth
                id="streetNumber"
                name="streetNumber"
                type="number"
                InputProps={{ inputProps: { min: 0 } }}
                value={formik.values.streetNumber}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={Boolean(
                  formik.errors.streetNumber && formik.touched.streetNumber
                )}
                helperText={
                  formik.errors.streetNumber && formik.touched.streetNumber
                    ? formik.errors.streetNumber
                    : ''
                }
              />
            </Grid>

            <Grid item xs={12}>
              <TextField
                label="Referencia (opcional)"
                fullWidth
                id="reference"
                name="reference"
                value={formik.values.reference}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={Boolean(
                  formik.errors.reference && formik.touched.reference
                )}
                helperText={
                  formik.errors.reference && formik.touched.reference
                    ? formik.errors.reference
                    : ''
                }
              />
            </Grid>

            <Grid item xs={12}>
              <Typography variant="h5" sx={{ mb: 2, mt: 2 }}>
                Método de pago
              </Typography>
            </Grid>

            <Grid item xs={12}>
              <FormControl fullWidth>
                <InputLabel id="payment-method-label">Forma de pago</InputLabel>
                <Select
                  labelId="paymentMethod-label"
                  id="paymentMethod"
                  name="paymentMethod"
                  value={formik.values.paymentMethod}
                  onChange={(event) =>
                    handleChangePaymentMethod(event, formik.handleChange)
                  }
                  onBlur={formik.handleBlur}
                  error={Boolean(
                    formik.errors.paymentMethod && formik.touched.paymentMethod
                  )}
                >
                  <MenuItem value="cash">Efectivo</MenuItem>
                  <MenuItem value="visa">Tarjeta Visa</MenuItem>
                </Select>
              </FormControl>
            </Grid>

            {paymentMethod === 'cash' && (
              <Grid item xs={12}>
                <TextField
                  label="Monto a abonar"
                  fullWidth
                  id="cashValue"
                  name="cashValue"
                  type="number"
                  InputProps={{ inputProps: { min: 0 } }}
                  value={formik.values.cashValue}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={Boolean(
                    formik.touched.cashValue &&
                      (formik.errors.cashValue ||
                        totalOrder > parseInt(formik.values.cashValue))
                  )}
                  helperText={
                    formik.touched.cashValue &&
                    (formik.errors.cashValue ||
                      totalOrder > parseInt(formik.values.cashValue))
                      ? formik.errors.cashValue ||
                        'Monto a abonar menor al de la orden'
                      : ''
                  }
                />
              </Grid>
            )}

            {paymentMethod === 'visa' && (
              <>
                <Grid item xs={12} sm={6}>
                  <TextField
                    label="Número de tarjeta"
                    fullWidth
                    id="cardNumber"
                    name="cardNumber"
                    value={formik.values.cardNumber}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={Boolean(
                      formik.errors.cardNumber && formik.touched.cardNumber
                    )}
                    helperText={
                      formik.errors.cardNumber && formik.touched.cardNumber
                        ? formik.errors.cardNumber
                        : ''
                    }
                  />
                </Grid>

                <Grid item xs={12} sm={6}>
                  <TextField
                    label="Nombre y apellido del titular"
                    fullWidth
                    id="cardNames"
                    name="cardNames"
                    value={formik.values.cardNames}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={Boolean(
                      formik.errors.cardNames && formik.touched.cardNames
                    )}
                    helperText={
                      formik.errors.cardNames && formik.touched.cardNames
                        ? formik.errors.cardNames
                        : ''
                    }
                  />
                </Grid>

                <Grid item xs={12} sm={6}>
                  <TextField
                    label="Fecha de vencimiento (MM/YYYY)"
                    fullWidth
                    id="cardDueDate"
                    name="cardDueDate"
                    value={formik.values.cardDueDate}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={Boolean(
                      formik.errors.cardDueDate && formik.touched.cardDueDate
                    )}
                    helperText={
                      formik.errors.cardDueDate && formik.touched.cardDueDate
                        ? formik.errors.cardDueDate
                        : ''
                    }
                  />
                </Grid>

                <Grid item xs={12} sm={6}>
                  <TextField
                    label="CVC"
                    fullWidth
                    id="cardCvc"
                    name="cardCvc"
                    InputProps={{ inputProps: { min: 0 } }}
                    value={formik.values.cardCvc}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={Boolean(
                      formik.errors.cardCvc && formik.touched.cardCvc
                    )}
                    helperText={
                      formik.errors.cardCvc && formik.touched.cardCvc
                        ? formik.errors.cardCvc
                        : ''
                    }
                  />
                </Grid>
              </>
            )}

            <Grid item xs={12}>
              <Typography variant="h5" sx={{ mb: 2, mt: 2 }}>
                Entrega
              </Typography>
            </Grid>

            <Grid item xs={12}>
              <FormControl fullWidth>
                <InputLabel id="shipType-label">Fecha de entrega</InputLabel>
                <Select
                  labelId="shipType-label"
                  id="shipMethod"
                  name="shipMethod"
                  value={formik.values.shipMethod}
                  onChange={(event) =>
                    handleChangeShipType(event, formik.handleChange)
                  }
                  onBlur={formik.handleBlur}
                  error={Boolean(
                    formik.errors.shipMethod && formik.touched.shipMethod
                  )}
                >
                  <MenuItem value="asap">Lo antes posible</MenuItem>
                  <MenuItem value="date">Fecha determinada</MenuItem>
                </Select>
              </FormControl>
            </Grid>

            {shipType === 'date' && (
              <Grid item xs={12}>
                <TextField
                  label="Fecha"
                  type="datetime-local"
                  style={{ width: '50%', minWidth: '328px' }}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  id="shipDate"
                  name="shipDate"
                  value={formik.values.shipDate}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={Boolean(
                    formik.errors.shipDate && formik.touched.shipDate
                  )}
                  helperText={
                    formik.errors.shipDate && formik.touched.shipDate
                      ? formik.errors.shipDate
                      : ''
                  }
                />
              </Grid>
            )}

            <Grid
              item
              xs={12}
              style={{ display: 'flex', justifyContent: 'center' }}
            >
              <Button
                type="submit"
                variant="contained"
                color="primary"
                size="large"
                disabled={totalOrder === 0}
              >
                Realizar pedido
              </Button>
            </Grid>
          </Grid>
        </Form>
      )}
    </Formik>
  );
};

export default ShoppingCartForm;
