import * as Yup from 'yup';

const formValidationSchema = Yup.object({
  city: Yup.string().required('Campo requerido'),
  street: Yup.string().required('Campo requerido'),
  streetNumber: Yup.number().required('Campo requerido'),
  reference: Yup.string().optional(),
  paymentMethod: Yup.string().required('Campo requerido'),
  cashValue: Yup.number().required('Campo requerido'),
  cardNumber: Yup.string()
    .when('paymentMethod', (paymentMethod, schema) => {
      return paymentMethod === 'visa'
        ? schema.required('Campo requerido')
        : schema;
    })
    .matches(/^4[0-9]{12}(?:[0-9]{3})?$/, 'Número de tarjeta inválido'),
  cardNames: Yup.string().when('paymentMethod', (paymentMethod, schema) => {
    return paymentMethod === 'visa'
      ? schema.required('Campo requerido')
      : schema;
  }),
  cardDueDate: Yup.string()
    .when('paymentMethod', (paymentMethod, schema) => {
      return paymentMethod === 'visa'
        ? schema.required('Campo requerido')
        : schema;
    })
    .matches(/^(0[1-9]|1[0-2])\/?([0-9]{4})$/, 'Formato inválido (MM/YYYY)'),
  cardCvc: Yup.string()
    .when('paymentMethod', (paymentMethod, schema) => {
      return paymentMethod === 'visa'
        ? schema.required('Campo requerido')
        : schema;
    })
    .matches(/^\d{3}$/, 'Formato inválido'),
  shipMethod: Yup.string().required('Campo requerido'),
  shipDate: Yup.date()
    .when('shipMethod', (shipMethod, schema) => {
      return shipMethod === 'date'
        ? schema.required('Campo requerido')
        : schema;
    })
    .min(new Date(), 'Fecha inválida'),
});

export default formValidationSchema;
