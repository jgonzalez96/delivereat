import './not-found.styles.css';

import { Container, Typography } from '@mui/material';
import React, { ReactElement } from 'react';

import { Box } from '@mui/system';
import Header from '../../components/header/header';
import ReportIcon from '@mui/icons-material/Report';

const NotFoundView: React.FC = (): ReactElement => {
  return (
    <>
      <Header />

      <Container maxWidth="md" sx={{ mt: 5, mb: 5 }}>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <ReportIcon color="error" sx={{ fontSize: 80 }} />
          <Typography variant="h3" align="center">
            Página no encontrada
          </Typography>
        </Box>
      </Container>
    </>
  );
};

export default NotFoundView;
