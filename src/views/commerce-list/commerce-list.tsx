import './commerce-list.styles.css';

import { Container, Typography } from '@mui/material';
import React, { ReactElement } from 'react';

import CommerceList from '../../components/commerce-list/commerce-list';
import Header from '../../components/header/header';

const CommerceListView: React.FC = (): ReactElement => {
  return (
    <>
      <Header />

      <Container maxWidth="md" sx={{ mt: 5, mb: 5 }}>
        <Typography variant="h4" align="center" sx={{ mb: 5 }}>
          Elija uno de nuestros comercios adheridos
        </Typography>
        <CommerceList />
      </Container>
    </>
  );
};

export default CommerceListView;
