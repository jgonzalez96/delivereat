import './shopping-cart.styles.css';

import { Container, Typography } from '@mui/material';
import React, { ReactElement, useState } from 'react';

import Header from '../../components/header/header';
import ProductsTable from '../../components/products-table/products-table';
import ShoppingCartForm from '../../components/shopping-cart-form/shopping-cart-form';

const ShoppingCartView: React.FC = (): ReactElement => {
  const [productsChanged, setProductsChanged] = useState(0);

  return (
    <>
      <Header />

      <Container maxWidth="md" sx={{ mt: 5, mb: 5 }}>
        <Typography variant="h4" align="center" sx={{ mb: 5 }}>
          Carrito de compra
        </Typography>
        <Typography variant="h5" sx={{ mb: 5 }}>
          Productos
        </Typography>
        <ProductsTable setProductsChanged={setProductsChanged} />
        <ShoppingCartForm productsChanged={productsChanged} />
      </Container>
    </>
  );
};

export default ShoppingCartView;
