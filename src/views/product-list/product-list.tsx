import './product-list.styles.css';

import { Container, Typography } from '@mui/material';
import React, { ReactElement, useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';

import Header from '../../components/header/header';
import { Product } from '../../data/types';
import ProductList from '../../components/product-list/product-list';
import commerceProducts from '../../data/commerce-products.json';
import commerces from '../../data/commerces.json';

const ProductListView: React.FC = (): ReactElement => {
  const { commerceId } = useParams();
  const navigate = useNavigate();

  const [commerce, setCommerce] = useState<string | undefined | null>(null);
  const [products, setProducts] = useState<Product[]>([]);

  useEffect(() => {
    const foundCommerce = commerces.find(
      (commerce) => commerce.id === parseInt(commerceId as string)
    );
    setCommerce(foundCommerce?.name);
  }, [commerceId]);

  useEffect(() => {
    const foundProducts = commerceProducts.filter(
      (commerceProduct) =>
        commerceProduct.commerceId === parseInt(commerceId as string)
    );
    if (foundProducts.length === 0) {
      navigate('/');
    }
    setProducts(foundProducts);
  }, [commerceId, navigate]);

  return (
    <>
      <Header />

      <Container maxWidth="md" sx={{ mt: 5, mb: 5 }}>
        <Typography variant="h4" align="center" sx={{ mb: 5 }}>
          Productos de {commerce}
        </Typography>
        <ProductList products={products} />
      </Container>
    </>
  );
};

export default ProductListView;
